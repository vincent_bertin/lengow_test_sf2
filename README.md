# Test technique sf2 Lengow - Vincent Bertin

Commentaires sur les différentes tâches

1. ~
2. Le fichier de configuration avec "url_orders" est "resources.yaml".
3. ~
4. 
5. 
6. Cliquer sur le bouton vert "Mettre à jour" en haut à droite déclenchera le chargement du fichier XML, et affichera les commandes dans un tableau APYDataGridBundle.
7. Le formulaire pour ajouter une commande est accessible via la route racine, ou en cliquant sur "Ajouter une commande" dans la navigation.
8. Le JSON de toutes les commandes est accessible via la route demandée (/api), mais aussi en allant dans "Toutes les commandes" > "JSON" (bouton bleu à droite du titre "Commandes")
9. Même logique que pour la tâche précédente
10. Je n'ai malheureusement pas trouvé le moyen d'afficher mes données en YAML.
