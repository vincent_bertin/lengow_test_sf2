<?php

namespace Lengow\TestBundle\Load;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelectorConverter;
use Lengow\TestBundle\Entity\LengowOrder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class LengowLoad
{
  /**
  * EntityManager
  * @var EntityManager
  */
  protected $em;

  /**
  * Logger
  * @var LoggerInterface
  */
  protected $logger;

  /**
  * Url fichier xml
  */
  protected $urlOrders;

  public function __construct(EntityManager $entityManager, LoggerInterface $logger, $urlOrders)
  {
    $this->em = $entityManager;
    $this->logger = $logger;
    $this->urlOrders = $urlOrders;
  }

  public function loadOrders()
  {
    $logger = $this->logger;
    $manager = $this->em;
    $urlOrders = $this->urlOrders;
    $repository = $manager->getRepository("LengowTestBundle:LengowOrder");
    $xmlFile = @file_get_contents($urlOrders);
    if ($xmlFile === false)
    {
      $logger->error("Erreur à l'ouverture de ". $urlOrders);
      return (FALSE);
    }
    $logger->info("Le fichier ". $urlOrders ." est ouvert");
    $xml = new Crawler();
    $xml->addXmlContent($xmlFile);
    $orders = $xml->filter("orders order");
    $orders->each(function (Crawler $order) use ($manager, $repository, $logger, &$result)
    {
      $lengowOrder = $this->getOrderFromXml($order);
      $exists = $repository->findBy(array("orderId" => $lengowOrder->getOrderId()));
      if ($exists)
        $logger->error("La commande #". $lengowOrder->getOrderId() ." existe déjà");
      else
      {
        $logger->info("La commande #". $lengowOrder->getOrderId() ." a été enregistrée");
        $manager->persist($lengowOrder);
      }
    });
    $manager->flush();
    $logger->info("Fin de lecture du fichier ". $urlOrders);
    return (TRUE);
  }

  private function getOrderFromXml(Crawler $order)
  {
    $lengowOrder = new LengowOrder;
    $orderId = $order->filter("order_id")->text();
    $billingEmail = $order->filter("billing_email")->text();
    $orderAmount = $order->filter("order_amount")->text();
    $orderZipCode = $order->filter("billing_zipcode")->text();
    if ($orderZipCode == "None")
      $orderZipCode = 0;
    $lengowOrder->setOrderId($orderId);
    $lengowOrder->setBillingEmail($billingEmail);
    $lengowOrder->setOrderAmount($orderAmount);
    $lengowOrder->setBillingZipCode($orderZipCode);
    return ($lengowOrder);
  }

  public function getOrders($format = NULL)
  {
    $manager = $this->em;
    $repository = $manager->getRepository("LengowTestBundle:LengowOrder");
    $orders = $repository->findAll();
    return ($this->returnByFormat($orders, $format));
  }

  public function getOrder($id, $format = NULL)
  {
    $manager = $this->em;
    $repository = $manager->getRepository("LengowTestBundle:LengowOrder");
    $order = $repository->findBy(array('orderId' => $id));
    return ($this->returnByFormat($order, $format));
  }

  private function returnByFormat($data, $format)
  {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    switch ($format)
    {
      case "json":
        return ($serializer->serialize($data, 'json'));
      default:
        return ($data);
    }
    return ($data);
  }
}
