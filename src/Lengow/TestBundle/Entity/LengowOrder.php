<?php

namespace Lengow\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * LengowOrder
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lengow\TestBundle\Entity\LengowOrderRepository")
 * @UniqueEntity(fields="orderId", message="Une commande existe déjà avec cet id.")
 * @GRID\Source(columns="id, orderId, billingEmail, orderAmount, billingZipCode")
 */
class LengowOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @GRID\Column(visible=false)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     * @GRID\Column(title="ID de la commande")
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_email", type="string", length=255)
     * @Assert\Email(message="L'adresse e-mail n'est pas valide")
     * @GRID\Column(title="Mail du client")
     */
    private $billingEmail;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     * @Assert\Type(type="real", message="Le montant doit être un chiffre")
     * @GRID\Column(title="Prix (€)")
     */
    private $orderAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="billing_zipcode", type="integer")
     * @Assert\Type(type="integer", message="Code postal invalide")
     * @Assert\Length(min=5, max=5, exactMessage="Le code postal doit avoir 5 chiffres")
     * @GRID\Column(title="Code postal")
     */
    private $billingZipCode;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return LengowOrder
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set billingEmail
     *
     * @param string $billingEmail
     * @return LengowOrder
     */
    public function setBillingEmail($billingEmail)
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    /**
     * Get billingEmail
     *
     * @return string
     */
    public function getBillingEmail()
    {
        return $this->billingEmail;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return LengowOrder
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set billingZipCode
     *
     * @param integer $billingZipCode
     * @return LengowOrder
     */
    public function setBillingZipCode($billingZipCode)
    {
        $this->billingZipCode = $billingZipCode;

        return $this;
    }

    /**
     * Get billingZipCode
     *
     * @return integer
     */
    public function getBillingZipCode()
    {
        return $this->billingZipCode;
    }
}
