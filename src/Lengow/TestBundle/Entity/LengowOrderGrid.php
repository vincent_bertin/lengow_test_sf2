<?php
namespace Lengow\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
* @GRID\Source(columns="id, my_datetime")
**/
class LengowOrderGrid
{
  /**
  * @ORM\Column(type="integer")
  **/
  protected $id;

 /**
  * @ORM\Column(type="datetime")
  **/
	protected $my_datetime;
}
