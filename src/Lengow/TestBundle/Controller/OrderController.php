<?php

namespace Lengow\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Lengow\TestBundle\Entity\LengowOrder;
use Lengow\TestBundle\Form\LengowOrderType;
use APY\DataGridBundle\Grid\Source\Entity;

class OrderController extends Controller
{
    public function viewAllAction($format = "json")
    {
      $lengow_test = $this->container->get("lengow_test");
      $orders = $lengow_test->getOrders($format);
      return ($this->getResponseByFormat($orders, $format));
    }

    public function viewAction($format = "json", $id_order)
    {
      $lengow_test = $this->container->get("lengow_test");
      $order = $lengow_test->getOrder($id_order, $format);
      return ($this->getResponseByFormat($order, $format));
    }

    private function getResponseByFormat($data, $format)
    {
      switch ($format)
      {
        case "json":
          $response = new Response($data);
          $response->headers->set('Content-Type', 'application/json');
          break;
        default:
          $response = new Response($orders);
          break;
      }
      return ($response);
    }

    public function loadXmlAction(Request $request)
    {
      $lengow_test = $this->container->get("lengow_test");
      $data = $lengow_test->loadOrders();
      if ($data)
        $request->getSession()->getFlashBag()->add('notice', 'Données mises à jour');
      else
        $request->getSession()->getFlashBag()->add('notice', 'Erreur lors de la mise à jour des données');
      $source = new Entity('LengowTestBundle:LengowOrder');
      $grid = $this->get('grid');
      $grid->setSource($source);
      return ($grid->getGridResponse('LengowTestBundle:Default:orders.html.twig'));
    }

    public function addOrderAction(Request $request)
    {
      $order = new LengowOrder;
      $form = $this->createForm(new LengowOrderType(), $order);
      $form->handleRequest($request);
      if ($form->isValid())
      {
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($order);
        $manager->flush();
        $request->getSession()->getFlashBag()->add('notice', 'Commande enregistrée.');
      }
      return $this->render('LengowTestBundle:Default:addOrder.html.twig', array(
        'form' => $form->createView(),
      ));
    }

    public function viewHomepageAction()
    {
      $lengow_test = $this->get('lengow_test');
      $orders = $lengow_test->getOrders();
      return ($this->render("LengowTestBundle:Default:viewHomepage.html.twig", array('orders' => $orders)));
    }
}
